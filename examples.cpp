#include "range/v3/all.hpp"
#include <algorithm>
#include <iostream>

using namespace std;

int main() {
  // Task: remove odd numbers and then multiply the remaining numbers with 10

  // Alternative 1, using dangerous for-loop
  {
    vector<int> numbers = {4, 1, 3, 7, 2};
    for (auto it = numbers.begin(); it != numbers.end();) {
      if (*it % 2 == 1)
        it = numbers.erase(it);
      else {
        *it *= 10;
        ++it;
      }
    }
    for (auto &number : numbers)
      cout << number << endl;
  }

  // Alternative 2, using complicated algorithm functions
  {
    vector<int> numbers = {4, 1, 3, 7, 2};
    numbers.erase(remove_if(numbers.begin(), numbers.end(), [](int e) { return e % 2 == 1; }), numbers.end());
    transform(numbers.begin(), numbers.end(), numbers.begin(), [](int e) { return e * 10; });
    for (auto &number : numbers)
      cout << number << endl;
  }

  // Alternative 3, using ranges, and without altering numbers
  {
    vector<int> numbers = {4, 1, 3, 7, 2};
    auto view = numbers | ranges::view::remove_if([](int e) { return e % 2 == 1; }) |
                ranges::view::transform([](int e) { return e * 10; });
    for (const auto &number : view)
      cout << number << endl;
  }
}
