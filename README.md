# Example C++ application using ranges

## Prerequisites
The C++ IDE [juCi++](https://gitlab.com/cppit/jucipp) should be installed.

## Compiling and running
```sh
git clone --recursive https://gitlab.com/ntnu-tdat3020/ranges
juci ranges
```

Choose Compile and Run in the Project menu.
